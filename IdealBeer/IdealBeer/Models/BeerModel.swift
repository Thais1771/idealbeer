//
//  BeerModel.swift
//  IdealBeer
//
//  Created by Thais Rodriguez on 02/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import Foundation
import SwiftUI

struct Beer: Codable, Identifiable {
    let id = UUID()
    let name: String
    let tagLine: String
    let description: String
    let imageURL: String
    let ABV: Float
    
    private enum CodingKeys: String, CodingKey {
        case name = "name"
        case tagLine = "tagline"
        case description = "description"
        case imageURL = "image_url"
        case ABV = "abv"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        tagLine = try values.decode(String.self, forKey: .tagLine)
        description = try values.decode(String.self, forKey: .description)
        if let imageURL = try? values.decode(String.self, forKey: .imageURL) {
            self.imageURL = imageURL
        } else {
            self.imageURL = ""
        }
        ABV = try values.decode(Float.self, forKey: .ABV)
    }
}
