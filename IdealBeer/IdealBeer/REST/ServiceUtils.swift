//
//  ServiceUtils.swift
//  IdealBeer
//
//  Created by Thais Rodriguez on 02/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class ServiceUtils: NSObject {
    
    func getBeers(url: String,  completion: @escaping(_ return: [Beer]) -> Void) {
        guard let url = URL(string: url) else {return}
        URLSession.shared.dataTask(with: url) { data, response, error in
            do {
                let data = try JSONDecoder().decode([Beer].self, from: data!)
                DispatchQueue.main.async {
                    completion(data)
                }
            } catch let error as NSError {
                debugPrint("Error: \(error.description)")
            }
        }.resume()
    }
    
    func constructSearchByFood(_ food: String) -> String {
        return food == "" ? "https://api.punkapi.com/v2/beers" : "https://api.punkapi.com/v2/beers?food=\(food)"
    }
}
