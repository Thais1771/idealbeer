//
//  MainViewModel.swift
//  IdealBeer
//
//  Created by Thais Rodriguez on 02/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

class MainViewModel: ObservableObject {
    @Published var data = [Beer]()
    var sortAscendant = true
    let service = ServiceUtils()
    
    init() {
        service.getBeers(url: service.constructSearchByFood(""), completion: { data in
            self.data = data
            self.sortData(false)
        })
    }
    
    func searchButtonAction(food: String) {
        let direction: String = service.constructSearchByFood(food)
        service.getBeers(url: direction, completion: { data in
            self.data = data
            self.sortAscendant = true
            self.sortData(false)
        })
    }
    
    func sortData(_ fromSortButton: Bool) {
        if fromSortButton { self.sortAscendant = !self.sortAscendant }
        self.sortAscendant ? self.data.sort(by: {$0.ABV < $1.ABV}) : self.data.sort(by: {$0.ABV > $1.ABV})
    }
}
