//
//  MainView.swift
//  IdealBeer
//
//  Created by Thais Rodriguez on 02/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

//SEPARALO EN VISTAS, ANIMAL
struct MainView: View {
    @State var foodToSearch: String = ""
    @ObservedObject var viewModel = MainViewModel()
    @State var ascendantSort = false
    
    var body: some View {
        NavigationView {
            VStack {
                //NavBar
                //With search bar and resorted array elements
                HStack {
                    //SearchBar
                    TextField("Enter some text", text: $foodToSearch)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .foregroundColor(Color("principalColor"))
                    //SearchButton
                    Button(action: {
                        self.viewModel.searchButtonAction(food: self.foodToSearch)
                    }) {
                        Text("Search")
                    }.foregroundColor(Color("principalColor"))
                }.padding()
                //BeerCells or EmptyView
                if self.viewModel.data.isEmpty {
                    EmptyView()
                } else {
                    List(viewModel.data) { item in
                        BeerCell(data: item)
                    }
                }
            }
                //Navigation barTittle and barItem
                .navigationBarTitle(Text("Your ideal beer"))
                .navigationBarItems(trailing: Button(action: {
                    self.viewModel.sortData(true)
                    self.ascendantSort = !self.ascendantSort
                }){
                    Image(systemName: "arrow.up.arrow.down.square.fill")
                        .resizable()
                        .frame(width: 25, height:25)
                        .foregroundColor(Color("principalColor"))
                })
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
