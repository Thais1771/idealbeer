//
//  LottieView.swift
//  IdealBeer
//
//  Created by Thais Rodríguez on 07/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import Foundation
import Lottie
import SwiftUI

final class LottieView: UIViewRepresentable, ObservableObject {
    
    @Published var animationEnded = false
    
    let animationView = AnimationView()
    var fileName: String
    
    init(fileName: String) {
        self.fileName = fileName
    }
    
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<LottieView>) {
    }
    
    func makeUIView(context: UIViewRepresentableContext<LottieView>) -> UIView {
        let view = UIView()
        
        animationView.animation = Animation.named(fileName)
        animationView.contentMode = .scaleAspectFill
        animationView.play { _ in
            self.animationEnded = true
        }
        
        animationView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(animationView)
        
        NSLayoutConstraint.activate([
            animationView.widthAnchor.constraint(equalTo: view.widthAnchor),
            animationView.heightAnchor.constraint(equalTo: view.heightAnchor)
        ])
        
        return view
    }
}
