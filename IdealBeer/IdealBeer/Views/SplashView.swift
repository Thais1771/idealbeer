//
//  SplashView.swift
//  IdealBeer
//
//  Created by Thais Rodríguez on 08/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import SwiftUI

struct SplashView: View {
    var splashAnimation = LottieView(fileName: "bubbleLoader")
    
    var body: some View {
        ZStack {
            Color("principalColor")
            splashAnimation
                .frame(width: 300, height: 300)
        }.edgesIgnoringSafeArea(.all)
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
