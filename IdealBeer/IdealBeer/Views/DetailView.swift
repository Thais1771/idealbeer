//
//  DetailView.swift
//  IdealBeer
//
//  Created by Thais Rodríguez on 08/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct DetailView: View {
    let data: Beer
    
    var body: some View {
        ZStack {
            VStack{
                WebImage(url: URL(string: data.imageURL))
                    .resizable()
                    .placeholder(Image("beerPlaceholder")) // Placeholder Image
                    .scaledToFit()
                    .frame(height: UIScreen.main.bounds.width*0.7, alignment: .center)
                    .padding(5)
                VStack(alignment: .leading, spacing: 4) {
                    ScrollView {
                        VStack(alignment: .leading) {
                            //BeerTagLine
                            Text("Tag Line")
                                .bold()
                                .underline()
                            Text(data.tagLine)
                            //BeerDescription
                            Text("Description")
                                .bold()
                                .underline()
                            Text(data.description)
                            //BeerABV
                            Text("ABV: \(data.ABV,specifier: "%.2f")%")
                                .bold()
                            Spacer()
                        }.padding(10)
                            .foregroundColor(.black)
                    }
                }.background(Color("principalColor"))
                    .cornerRadius(15)
            }.padding(5)
        }.navigationBarTitle(data.name)
    }
}
