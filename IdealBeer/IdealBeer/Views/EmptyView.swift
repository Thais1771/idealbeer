//
//  EmptyView.swift
//  IdealBeer
//
//  Created by Thais Rodríguez on 08/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import SwiftUI

struct EmptyView: View {
    var body: some View {
        ZStack {
            Color("principalColor")
                .edgesIgnoringSafeArea(.all)
            VStack(alignment: .center){
                //EmptyState
                Image("emptyStateImage")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 300, height: 300, alignment: .center)
                Text("There are no beers for this meal")
                    .font(.system(size: 35, weight: .bold, design: .default))
                    .frame(width: UIScreen.main.bounds.width*0.8, alignment: .center)
                    .multilineTextAlignment(.center)
                    .foregroundColor(Color("secondaryColor"))
            }
        }
    }
}

struct EmptyView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView()
    }
}
