//
//  RouterView.swift
//  IdealBeer
//
//  Created by Thais Rodríguez on 08/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import SwiftUI

struct RouterView: View {
    
    @ObservedObject var splashAnimation = LottieView(fileName: "bubbleLoader")
    
    var body: some View {
        if self.splashAnimation.animationEnded {
            return AnyView( MainView())
        } else {
            return AnyView(
                SplashView(splashAnimation: splashAnimation)
        )}
    }
}

struct RouterView_Previews: PreviewProvider {
    static var previews: some View {
        RouterView()
    }
}
