//
//  BeerCell.swift
//  IdealBeer
//
//  Created by Thais Rodríguez on 08/03/2020.
//  Copyright © 2020 Thais Rodriguez. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct BeerCell: View {
    var data: Beer
    
    var body: some View {
        NavigationLink(destination: DetailView(data: data)) {
            HStack(alignment: .center, spacing: 10) {
                //Beer image
                WebImage(url: URL(string: data.imageURL))
                    .resizable()
                    .placeholder(Image("beerPlaceholder")) // Placeholder Image
                    .indicator(.activity) // Activity Indicator
                    .animation(.easeInOut(duration: 0.5)) // Animation Duration
                    .transition(.fade) // Fade Transition
                    .scaledToFit()
                    .frame(width: UIScreen.main.bounds.width*0.2, height: UIScreen.main.bounds.width*0.4)
//                    .padding(.leading, 5)
                //Beer features
                VStack(alignment: .leading, spacing: 4) {
                    //BeerName
                    Text(data.name)
                        .font(.system(size: 20, weight: .bold, design: .default))
                    //BeerTagLine
                    Text("Tag Line:")
                        .font(.caption)
                        .bold()
                        .underline()
                    Text(data.tagLine)
                    //BeerDescription
                    Text("Description:")
                        .font(.caption)
                        .bold()
                        .underline()
                    Text(data.description)
                        .lineLimit(6)
                    //BeerABV
                    Text("ABV: \(data.ABV,specifier: "%.2f")%")
                        .bold()
                }.padding(.trailing, 5)
            }
            .cornerRadius(15)
            .padding(.bottom, 10)
            .padding(.top, 10)
        }
    }
}
