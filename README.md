IdealBeer es una aplicación diseñada para saber que cerveza concuerda con lo que estés comiendo.

Cuando abres la apliación te salen todas las posibles cervezas que puedes combinar con tu comida. A partir de aquí puedes buscar, en la barra que hay en la parte superior de la lista, la comida para la que quieres encontrar la cerveza perfecta.
También puedes filtrar las cervezas por su ABV, tanto ascendente como descendentemente, pulsando en el botón que hay en la parte superior derecha de la pantalla.

En la vista principal la descripción de la cerveza tiene un maximo de 6 líneas, por tanto si quieres ver la cerveza con mas detalle solo tienes que pulsar en ella y se te abrira la vista detalle con la descripción completa.

Librerías utilizadas:
    SDWebImageSwiftUI: Esta librería se utiliza para cargar las imagenes de las cervezas, para ellos hay que pasarle la URL que se recoge de la API, también, en caso de que no pueda cargarla se le puede añadir un PlaceHolder.
    Lottie: Esta librería se utiliza para poder añadir un splash, para que cuando se habra la applicación se vea dicha animación.